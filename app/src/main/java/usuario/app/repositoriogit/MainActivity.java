package usuario.app.repositoriogit;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import usuario.app.repositoriogit.adapters.RepositorioAdapter;
import usuario.app.repositoriogit.eventos.EventoClickRepositorio;
import usuario.app.repositoriogit.interfaces.RepositorioServico;
import usuario.app.repositoriogit.modelo.BuscaDeRepositorio;
import usuario.app.repositoriogit.modelo.Repositorio;

public class MainActivity extends AppCompatActivity{

    private Retrofit retrofit;
    private ProgressDialog progressDialog;
    private RecyclerView rcView;
    private RepositorioAdapter mAdapter;
    private RepositorioServico service;
    public static final String QUERY_BUSCA = "language:Java";
    public static final String SORT = "stars";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rcView = (RecyclerView) findViewById(R.id.m_recycle_view);

        retrofit = new Retrofit.Builder()
                .baseUrl(RepositorioServico.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(RepositorioServico.class);

        configurarAdapter();

        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#3B3B3B")));
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void eventoResitorioClick(EventoClickRepositorio evento) {
        Repositorio repositorio = evento.getRepositorio();
        Intent intent = PullActivity.newIntent(this, repositorio);
        startActivity(intent);
    }

    public void configurarAdapter() {
        mAdapter = new RepositorioAdapter(this);
        rcView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rcView.setLayoutManager(layoutManager);
        rcView.setAdapter(mAdapter);
        rcView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                buscarRepositorios(page);
            }
        });
    }

    public boolean verificaConexao() {
        boolean conectado;
        ConnectivityManager conectivtyManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conectivtyManager.getActiveNetworkInfo() != null
                && conectivtyManager.getActiveNetworkInfo().isAvailable()
                && conectivtyManager.getActiveNetworkInfo().isConnected()) {
            conectado = true;
        } else {
            conectado = false;
        }
        return conectado;
    }

    private void buscarRepositorios(int pagina) {

        Call<BuscaDeRepositorio> retorno = service.buscarRepositorio(QUERY_BUSCA, SORT, String.valueOf(pagina));
        Log.d("Retrofit Teste: ", "antes da chamada ao web service");
        retorno.enqueue(new Callback<BuscaDeRepositorio>() {
            @Override
            public void onResponse(Call<BuscaDeRepositorio> call, Response<BuscaDeRepositorio> response) {
                Log.d("Retrofit Teste: ", "onResponse");
                Log.d("Retrofit Teste: ", response.code() + "");
                if (response.code() == 200) {
                    BuscaDeRepositorio buscaDeReceitas = response.body();
                    //mAdapter.setRepositorios(buscaDeReceitas.getRepositorios());
                    mAdapter.adicionarRepositorios(buscaDeReceitas.getRepositorios());
                    Log.d("Retrofit Teste: ", buscaDeReceitas.getTotalCount() + "");
                    Log.d("Retrofit Teste: ", buscaDeReceitas.getRepositorios().size() + "");
                } else {
                    Log.d("Retrofit Teste: ", "erro na requisição ");
                }
            }

            @Override
            public void onFailure(Call<BuscaDeRepositorio> call, Throwable t) {
                Log.d("Retrofit Teste: ", "onFailure");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.download) {
            if (verificaConexao() == true) {
                buscarRepositorios(1);
            } else {
                Toast.makeText(this, "Sem conexão de internet", Toast.LENGTH_LONG)
                        .show();
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
