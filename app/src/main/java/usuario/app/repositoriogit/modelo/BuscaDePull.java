package usuario.app.repositoriogit.modelo;

import java.util.List;

/**
 * Created by marcos.j.estagiario on 20/10/2016.
 */
public class BuscaDePull {

    private List<Pull> pulls;

    BuscaDePull() {

    }

    public List<Pull> getPulls() {
        return pulls;
    }

    public void setPulls(List<Pull> pulls) {
        this.pulls = pulls;
    }
}
