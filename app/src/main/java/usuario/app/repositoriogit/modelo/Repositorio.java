package usuario.app.repositoriogit.modelo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by marcos.j.estagiario on 19/10/2016.
 */
public class Repositorio {

    @SerializedName("name")
    private String nomeDoRepositorio;
    @SerializedName("description")
    private String descricaoDoRepositorio;
    @SerializedName("owner")
    private Usuario donoDoRepositorio;
    @SerializedName("stargazers_count")
    private int estrelas;
    @SerializedName("forks")
    private int forks;


    public Repositorio() {

    }

    public String getNomeDoRepositorio() {
        return nomeDoRepositorio;
    }

    public void setNomeDoRepositorio(String nomeDoRepositorio) {
        this.nomeDoRepositorio = nomeDoRepositorio;
    }

    public String getDescricaoDoRepositorio() {
        return descricaoDoRepositorio;
    }

    public void setDescricaoDoRepositorio(String descricaoDoRepositorio) {
        this.descricaoDoRepositorio = descricaoDoRepositorio;
    }

    public Usuario getDonoDoRepositorio() {
        return donoDoRepositorio;
    }

    public void setDonoDoRepositorio(Usuario donoDoRepositorio) {
        this.donoDoRepositorio = donoDoRepositorio;
    }

    public int getEstrelas() {
        return estrelas;
    }

    public void setEstrelas(int estrelas) {
        this.estrelas = estrelas;
    }

    public int getForks() {
        return forks;
    }

    public void setForks(int forks) {
        this.forks = forks;
    }
}
