package usuario.app.repositoriogit.modelo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by marcos.j.estagiario on 20/10/2016.
 */
public class Pull {

    @SerializedName("title")
    private String titulo;
    @SerializedName("body")
    private String body;
    @SerializedName("created_at")
    private String data;
    @SerializedName("user")
    private Usuario usuario;
    @SerializedName("state")
    private String estado;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
