package usuario.app.repositoriogit.modelo;

import com.google.gson.annotations.SerializedName;

import java.util.List;


/**
 * Created by marcos.j.estagiario on 19/10/2016.
 */
public class BuscaDeRepositorio {
    @SerializedName("total_count")
    private int totalCount;
    @SerializedName("items")
    private List<Repositorio> repositorios;

    public BuscaDeRepositorio() {

    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<Repositorio> getRepositorios() {
        return repositorios;
    }

    public void setRepositorios(List<Repositorio> repositorios) {
        this.repositorios = repositorios;
    }
}
