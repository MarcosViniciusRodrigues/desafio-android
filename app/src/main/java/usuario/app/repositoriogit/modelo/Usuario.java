package usuario.app.repositoriogit.modelo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by marcos.j.estagiario on 19/10/2016.
 */
public class Usuario {

    @SerializedName("login")
    private String nome;
    @SerializedName("avatar_url")
    private String urlImagem;

    public String getUrlImagem() {
        return urlImagem;
    }

    public void setUrlImagem(String urlImagem) {
        this.urlImagem = urlImagem;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
