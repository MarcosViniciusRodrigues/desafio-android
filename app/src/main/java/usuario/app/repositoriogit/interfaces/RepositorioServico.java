package usuario.app.repositoriogit.interfaces;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import usuario.app.repositoriogit.modelo.BuscaDeRepositorio;
import usuario.app.repositoriogit.modelo.Pull;
import usuario.app.repositoriogit.modelo.Usuario;

/**
 * Created by marcos.j.estagiario on 19/10/2016.
 */
public interface RepositorioServico {

    String BASE_URL = "https://api.github.com/";

    @GET("/search/repositories")
    Call<BuscaDeRepositorio> buscarRepositorio(@Query("q") String query, @Query("sort") String sort, @Query("page") String page);

    @GET("/repos/{creator}/{repository}/pulls")
    Call<List<Pull>> getRepositoryPullRequests(@Path("creator") String creator,
                                               @Path("repository") String reporitory);
}
