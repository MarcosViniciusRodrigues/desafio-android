package usuario.app.repositoriogit;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import usuario.app.repositoriogit.adapters.PullAdapter;
import usuario.app.repositoriogit.interfaces.RepositorioServico;
import usuario.app.repositoriogit.modelo.Pull;
import usuario.app.repositoriogit.modelo.Repositorio;

public class PullActivity extends AppCompatActivity {

    private Retrofit retrofit;
    private RecyclerView rcView;
    private PullAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private String donoDoRepositorio;
    private String nomeDoRepositorio;
    public static final String NOME_REPOSITORIO_ARG = "NOME_REPOSITORIO";
    public static final String NOME_DONO_REPOSITORIO_ARG = "NOME_DONO_REPOSITORIO";
    TextView open;
    TextView close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull);

        rcView = (RecyclerView) findViewById(R.id.m_recycle_view2);
        open = (TextView) findViewById(R.id.quantidade_opened);
        close = (TextView) findViewById(R.id.quantidade_closed);

        retrofit = new Retrofit.Builder()
                .baseUrl(RepositorioServico.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            donoDoRepositorio = extras.getString(NOME_DONO_REPOSITORIO_ARG);
            nomeDoRepositorio = extras.getString(NOME_REPOSITORIO_ARG);
        }

        configurarAdapter();

        RepositorioServico service = retrofit.create(RepositorioServico.class);
        Call<List<Pull>> retorno = service.getRepositoryPullRequests(donoDoRepositorio, nomeDoRepositorio);
        Log.d("Retrofit Teste: ", "antes da chamada ao web service");
        retorno.enqueue(new Callback<List<Pull>>() {
            @Override
            public void onResponse(Call<List<Pull>> call, Response<List<Pull>> response) {
                Log.d("Retrofit Teste: ", "onResponse");
                Log.d("Retrofit Teste: ", response.code() + "");
                if (response.code() == 200) {
                    List<Pull> pulls = response.body();
                    mAdapter.setPulls(pulls);
                    int aberto = 0;
                    int fechado = 0;
                    for (Pull pull : pulls) {
                        if (pull.getEstado().equals("open")) {
                            aberto++;
                        } else if (pull.getEstado().equals("close")) {
                            fechado++;
                        }
                    }
                    open.setText(String.valueOf(aberto));
                    close.setText(String.valueOf(fechado));
                    Log.d("Retrofit Teste: ", pulls.size() + "");
                } else {
                    Log.d("Retrofit Teste: ", "erro na requisição ");
                }
            }

            @Override
            public void onFailure(Call<List<Pull>> call, Throwable t) {
                Log.d("Retrofit Teste: ", "onFailure");
            }
        });
        Log.d("Retrofit Teste: ", "depois da chamada ao web service");

    }

    public void configurarAdapter() {
        mAdapter = new PullAdapter(this);
        rcView.setHasFixedSize(true);
        rcView.setLayoutManager(new LinearLayoutManager(this));
        rcView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pull, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static Intent newIntent(Context context, Repositorio repositorio) {
        Intent intent = new Intent(context, PullActivity.class);
        intent.putExtra(NOME_REPOSITORIO_ARG, repositorio.getNomeDoRepositorio());
        intent.putExtra(NOME_DONO_REPOSITORIO_ARG, repositorio.getDonoDoRepositorio().getNome());
        return intent;
    }
}
