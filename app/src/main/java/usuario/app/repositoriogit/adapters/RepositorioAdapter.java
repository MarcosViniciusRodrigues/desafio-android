package usuario.app.repositoriogit.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import usuario.app.repositoriogit.R;
import usuario.app.repositoriogit.eventos.EventoClickRepositorio;
import usuario.app.repositoriogit.modelo.Repositorio;

/**
 * Created by marcos.j.estagiario on 19/10/2016.
 */
public class RepositorioAdapter extends RecyclerView.Adapter<RepositorioAdapter.ViewHolder> {

    private List<Repositorio> repositorios = new ArrayList<>();
    private Context context;

    public RepositorioAdapter(Context context) {
        this.context = context;
    }

    public void setRepositorios(List<Repositorio> repositorios) {
        this.repositorios = repositorios;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_repositorio, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        Repositorio repositorio = repositorios.get(i);
        holder.txt_nome_repositorio.setText(repositorio.getNomeDoRepositorio());
        holder.txt_nome_descricao.setText(repositorio.getDescricaoDoRepositorio());
        holder.txt_fork.setText(String.valueOf(repositorio.getForks()));
        holder.txt_estrelas.setText(String.valueOf(repositorio.getEstrelas()));
        holder.txt_nome_sobrenome.setText(repositorio.getDonoDoRepositorio().getNome());
        Picasso.with(context).load(repositorio.getDonoDoRepositorio().getUrlImagem()).into(holder.avatar);

    }

    @Override
    public int getItemCount() {
        return repositorios.size();
    }

    public void adicionarRepositorios(List<Repositorio> repositorios) {
        this.repositorios.addAll(repositorios);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView txt_nome_repositorio;
        public TextView txt_nome_descricao;
        public TextView txt_fork;
        public TextView txt_estrelas;
        public ImageView avatar;
        public TextView txt_nome_sobrenome;
        public RecyclerView mRecyclerView;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_nome_repositorio = (TextView) itemView.findViewById(R.id.txt_nome_repositorio);
            txt_nome_descricao = (TextView) itemView.findViewById(R.id.txt_nome_descricao);
            txt_fork = (TextView) itemView.findViewById(R.id.txt_fork);
            txt_estrelas = (TextView) itemView.findViewById(R.id.txt_numero_estrela);
            txt_nome_sobrenome = (TextView) itemView.findViewById(R.id.txt_nome_sobrenome);
            avatar = (ImageView) itemView.findViewById(R.id.img_person);
            mRecyclerView = (RecyclerView) itemView.findViewById(R.id.m_recycle_view);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    Repositorio repositorio = repositorios.get(pos);
                    EventBus.getDefault()
                            .post(new EventoClickRepositorio(repositorio));
                }
            });

        }

        @Override
        public void onClick(View v) {


        }
    }
}
