package usuario.app.repositoriogit.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import usuario.app.repositoriogit.R;
import usuario.app.repositoriogit.modelo.Pull;
import usuario.app.repositoriogit.modelo.Repositorio;

/**
 * Created by marcos.j.estagiario on 20/10/2016.
 */
public class PullAdapter extends RecyclerView.Adapter<PullAdapter.ViewHolder> {

    private List<Pull> pulls = new ArrayList<>();
    private Context context2;

    public PullAdapter(Context context) {
        this.context2 = context;
    }

    public void setPulls(List<Pull> pulls) {
        this.pulls = pulls;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_nome_repositorio, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        Pull pull = pulls.get(i);
        holder.txt_titulo_repositorio.setText(pull.getTitulo());
        holder.txt_descricao_repositorio.setText(pull.getBody());
        Picasso.with(context2).load(pull.getUsuario().getUrlImagem()).into(holder.avata_repositorio);
        holder.login_repositorio.setText(pull.getUsuario().getNome());
        //Inserir Holder Quantidade Opened.
    }

    @Override
    public int getItemCount() {
        return pulls.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_titulo_repositorio;
        public TextView txt_descricao_repositorio;
        public ImageView avata_repositorio;
        public TextView login_repositorio;
        //public TextView quantidade_opened;


        public ViewHolder(View itemView) {
            super(itemView);
            txt_titulo_repositorio = (TextView) itemView.findViewById(R.id.txt_titulo_repositorio);
            txt_descricao_repositorio = (TextView) itemView.findViewById(R.id.txt_descricao_repositorio);
            avata_repositorio = (ImageView) itemView.findViewById(R.id.avata_repositorio);
            login_repositorio = (TextView) itemView.findViewById(R.id.login_repositorio);
            //quantidade_opened = (TextView) itemView.findViewById(R.id.quantidade_opened);

        }
    }
}
