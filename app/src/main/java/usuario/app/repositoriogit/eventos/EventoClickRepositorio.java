package usuario.app.repositoriogit.eventos;

import usuario.app.repositoriogit.modelo.Repositorio;

/**
 * Created by marcos.j.estagiario on 21/10/2016.
 */
public class EventoClickRepositorio {
    private final Repositorio repositorio;


    public EventoClickRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    public Repositorio getRepositorio() {
        return repositorio;
    }
}
